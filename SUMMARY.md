<!-- /*
 * File: /Users/pdillens/Uni/CBM/meetings-presentations/hitchhikers-guide-through-git/SUMMARY.md
 * Project: /Users/pdillens/Uni/CBM/meetings-presentations/hitchhikers-guide-through-git
 * Created Date: Wednesday November 13th 2019
 * Author: Pascal Raisig -- praisig@ikf.uni-frankfurt.de
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * license:
 * Copyright (c) 2019 Your Company
 */ -->

# Summary

* [Introduction](INTRO.md)
* [Readme](README.md)
* [Basics](BASICS.md)
  * [General setup](BASICS.md#setup)
  * [Workflow](BASICS.md#workflow)
  * [Commands](BASICS.md#commands)
* [Appendix]
  * [SetUp SSH](appendix/setUpSSH.md)
  * [CI/CD](appendix/cicd.md)
