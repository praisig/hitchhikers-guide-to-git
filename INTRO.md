# Hitchhikers guide to git

## Overview

This webpage is meant as a short guide for **git** beginners.  

Git is a versioning system for all kind of human readable files.  
Git manual:

    -Git - the stupid content tracker
    -================================
    +Git - fast, scalable, distributed revision control system

In our case it is mainly used to exchange and track the development of code or latex files. For example CbmRoot will be distributed via git from April 2020 on [CbmRootGitRepository](https://git.cbm.gsi.de/CbmSoft/CbmRoot).  
A project managed by git is also called **repository**.
A listing of basic commands will be given in the [Basic usage](BASICS.md) chapter. This chapter will also contain an introduction to the potential [workflows](BASICS.md/#workflow) used with git.
By the way this guide is also prepared and stored via git. It uses the so-called *gitbook* and *pages* functionalities.

If you are looking for a more extensive guide for analysis and co. in high energy physics, have a look at the [hitchhikers-guide-to-hep](https://github.com/dmb2/hitchhikers-guide-to-hep/blob/master/index.org)
