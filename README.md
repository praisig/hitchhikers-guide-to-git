# Readme

This guide is prepared with gitbook-cli for more information see [GitBook](https://docs.gitbook.com/)

---

To contribute to the guide please fork the guide and submit merge requests from your fork. Before submitting a merge request, please **ensure** that **your fork is up to date**. To do so, pull from this repository to your local git repository and push the update including your commit to your fork.

---

In this [README][readme] file you'll find a list of link definitions used throughout the guide. If you add new links to the document please add them as comments to the list! All links also have to be added at the bottom of the file in which they are used.

[readme]: https://gitlab.com/praisig/hitchhikers-guide-through-git/blob/master/README.md
