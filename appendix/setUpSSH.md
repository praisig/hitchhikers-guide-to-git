<!-- /*
 * File: /Users/pdillens/Uni/CBM/meetings-presentations/hitchhikers-guide-through-git/addendum/setUpSSH.md
 * Project: /Users/pdillens/Uni/CBM/meetings-presentations/hitchhikers-guide-through-git/addendum
 * Created Date: Wednesday November 13th 2019
 * Author: Pascal Raisig -- praisig@ikf.uni-frankfurt.de
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * license:
 * Copyright (c) 2019 Your Company
 */ -->

# Setup a ssh configuration

`github.com` has a quite nice [documentation](https://help.github.com/en/github/authenticating-to-github/connecting-to-github-with-ssh) for setting up ssh keys. Hence, for the time being this guide will refer to it.
