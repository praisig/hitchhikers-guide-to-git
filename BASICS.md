<!-- /*
 * File: /Users/pdillens/Uni/CBM/meetings-presentations/hitchhikers-guide-through-git/BASICS.md
 * Project: /Users/pdillens/Uni/CBM/meetings-presentations/hitchhikers-guide-through-git
 * Created Date: Wednesday November 13th 2019
 * Author: Pascal Raisig -- praisig@ikf.uni-frankfurt.de
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * license:
 * Copyright (c) 2019 Your Company
 */ -->

# Basic usage

First of all you have to ensure that you have git installed. In the case of the standard IKF desktop-machines it should be installed during the first setup. You can check the installation via `which git`.  
For *linux* machines git usually is available via the standard package manager. On *macOS* it is meanwhile part of the Xcode command-line tools.  

A git project can simply be *downloaded* but this also corresponds to only a download, i.e. you will not be able to use git features.
The standard procedure to access a git repository is a so-called **clone**. This command downloads the repository with all files needed to access it via git (see [cloning 101](#clone101)).

⚡️ **Before we start cloning/pushing/pulling/fetching and so on we have to understand the [workflows](#workflow) within git** and which of the potential workflows is used for the repository we want to access! ⚡️

## Git setup {#setup}

Git provides the possibility for global configuration and a local configuration, i.e. you can setup default values and repository correlated values. The global setup can be steered via the `~/.gitconfig` file.  
Most important settings:

```config
    [user]
        name = Max Mustermann
        email = max@mustermaenner.com
        github = maxmustermannsUsernameAtGithub
        gitlab = maxmustermannsUsernameAtGitlab
```

To define special settings for certain directories on your machine append the following to your `.gitconfig`:  

```config
    [includeIF "gitdir:/Your/Path/Here"]
    path = .gitconfig-SomeMeaningfulName
```

Your global settings are then extended/overwritten with those in the `.gitconfig-SomeMeaningfulName` file.

## How to access/create repositories from the command line {#userAccount}

There are two options to communicate with repositories:
    - ssh  
    - https

In principle **ssh** is preferable but needs some work [setting it up](appendix/setUpSSH.md). The setup has to be done on the website of the host for the git-repository and your computer.

# Workflow {#workflow}

There are mainly two and half different workflows for the integration and delivery of git repositories, they are discussed in [simple push and pull](#workflowPushAndPull) and [Merge/Pull requests](#workflowMergePullRequest).  
Both concepts allow the usage of **continuous integration (CI)** and **continuous delivery (CD)**. These features are discussed in the [CI/CD](appendix/cicd.md) chapter.  

Before discussing the integration and delivery we need to understand the concept of [branches](#branches).  

## Branches {#branches}

Within git the concept of branches is used. This is allows you for example to keep a stable release branch in your repository, while developing new experimental stuff on a development branch in the same repository.  
Usually the branch containing the latest version of a project is the **master** branch.
For your working environment you should **⚡️ never ⚡️** develop/change stuff on the master branch. Instead [create a development branch](#branch101) based on the branch/tag you want to start from.

## Simple push and pull concept {#workflowPushAndPull}

A simple access via push and pull for a selected amount of users (e.g. free-for-all, registered, invited). The repository itself has to be simply cloned and the users with access can directly push changes to the repository. Such a setup makes sense for small repositories with only a view people pushing changes, e.g. for writing a paper or small private code projects.

## Merge/Pull request concept {#workflowMergePullRequest}

To transfer updates to the repository a merge or pull request is required. Such a concept allows easier control of the changes pushed to the repository.

### Merge request

You access the repository via a standard clone of the repo itself. Whenever you have a commit which you want to push, you run `git push`. Your pushed commit will initialize a merge request, the user group allowed to sign off merge requests is informed about your request. They can now review your commit and comment on it, if they are fine with the changes they can start the merging.

### Pull request

Kind of similar to the merge request. However, this setup allows further disentanglement of the original repository from user changes.  
To submit changes to the original repository you have to do the following:
    1. Fork the original repository to your account (easiest done on the webpages of the git distributors)
    2. Clone the original repository to access the code (as in all other concepts)
    3. Write code, change things and so on
    4. Push all changes to your fork
    5. Submit a pull-request at the original repository (and by that initialize a similar procedure as after the git push in the merge request)

# The commands {#commands}

This section introduces the basic commands used within git.

[Get a local copy of the repository](#clone101): `git clone`  
[Create a branch/workdir to work on/in](#branch101): `git checkout -b` or `git worktree add`  
[Check the amount of local changes](#status101): `git status`  
[Commit your code or changes](#commit101): `git commit`  
[Temporary discard your changes](#pull101): `git stash`  
[Get remote changes](#pull101): `git pull`  
[Push your changes to the remote repository](#push101): `git push`  
<!-- `git checkout` -->

## You want to clone an existing repository {#clone101}

Probably the first thing where you will get in touch with git is cloning an existing repository:

```bash
    git clone https://gitlab.com/praisig/hitchhikers-guide-through-git.git
    git clone git@gitlab.com:praisig/hitchhikers-guide-to-git.git
```

The first version refers to a clone via https, the second to a clone via ssh.  
On a low level this all you need to get started with git clone!  

But at certain points a bit more knowledge will help to not use unnecessary large amount of resources:
So what does git clone actually do:
    - runs `git init` to initialize the current folder as git-directory
    - checkout of the branch the remote HEAD is pointing to by default

## Create/change branches {#branch101}

By default `git clone` initializes the default branch of the remote repository.
If you want to create another branch or use a different remote branch, for example to prepare/get a single fix and test it, `git checkout -b` is the command you are looking for.

```bash
    git checkout -b "your_new_or_remote_branch_name_here"
```

Note, if you *checkout* an existing branch this will *change* the content of the files and your local directory to the one of the selected branch.  
Often you want to have physically separated files for different branches (at least I prefer it).
Therefore, you can use a different command, which initializes a new branch in a separate directory.  
Former versions of git (might still be running on some of our machines, or some remote machines) need the additional **git-new-workdir package**:

```bash
    git-new-workdir "path_of_your_git_repo" "your_target_path" "desired_branch"
```

Lately git-new-workdir was kind of integrated into git.
Thus, now you can use **git worktree**:

```bash
    cd into_your_gitrepo
    git worktree add --force "your_target_path" "your_branch_name_here"
```

Note, the `--force` option is only needed if you want to add a branch to your worktree that already exists (e.g. you want to have another copy of the master branch).

`git worktree add` has a big advantage compared to `git-new-workdir`, while the latter copies the complete git repository and then checks out the given branch, the former creates a directory only with the content of the branch and links it to the original .git directory of the original local repository.

## Status of the repository {#status101}

Whenever you are in a directory of a git repository `git status` will show all local files/folders/etc. that contain changes compared to the HEAD (latest status) of the branch you are on.  
Note, that `git status` will not show you automatically if something in the remote repository has changed!

## Commit your code {#commit101}

Whenever you have changed/added/deleted something in the repository you have to commit you changes.
This is done via `git commit`.  
Before you can commit your code, you have to select the changes you want to put into your commit via:

```bash
    git add "your_changed_filename_here"
```

To select a certain file.
Note, *wildcards* are allowed.

```bash
    git add .
```

To add all changes displayed by `git status`.

Now that you have **staged** your changes for a commit, you can finally create your commit via `git commit`.
A commit is coupled to a **commit message**, a short message which reports on your changes.
The first line of the message should not me longer than 50 characters, if you need to add further information you can add another 72 characters after an empty line.
These 72 characters will only be shown, if one takes a detailed look at the commit.  
By default, git opens your default text editor for you to enter your commit message when you run `git commit`.
You can configure git to open another for example lighter editor, e.g. the default system editor on a mac is Xcode, which is quite heavy to open and only write 50 chars, hence, it makes sense to you use vim or something similar.

Actual commands and interesting options:

```bash
    git commit -a -m "\"your commit message here\""
```

The options:  
    - `-a` adds all unstaged changes (one can skip `git add`), but not files/folders new to the repository. One needs to add them explicitly via `git add`
    - `-m "\"your commit message here\""` allows you to write your message directly in the command line.

## Pull remote changes {#pull101}

To get updates from the remote repository one has to run `git pull --rebase`.
This command should be run quite frequently, to prevent your local repository from diverging from the remote repository.
Because, the more diverged repositories are, the more work it is to get them back in sync!
**Before** you run `git pull` you have to have handled your staged and unstaged changes, in git language *clean your working directory*.
Either, you [commit](#commit101) them, or you have to stash them.
Stashing your changes will remove them from your working directory into a kind of an attic.

```bash
    git stash -u
    git pull --rebase
    git stash pop
```

The options:
    - `-u` stashes also unstaged changes
    - `--rebase` puts your local commits at the top of the status tree
    - `pop` puts you stashed stuff back into the files

Note, I strongly recommend to use always use the `--rebase` option when pulling from remote repositories!

## Push changes to the remote repository {#push101}